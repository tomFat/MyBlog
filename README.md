# MyBlog

## 前言

这是我的个人技术博客，这个博客页面简约优雅，但是功能强大。

*演示站点*：<http://www.xingximing.cn/>

## 技术选型

名称 | 技术 | 版本
--------- | ------------- | -------------
语言 | Java | 1.8
框架 | SpringBoot | 1.5.1.RELEASE
ORM | Mybatis | 3.4.2
模板引擎 | Thymeleaf | 2.1.5.RELEASE
日志 | Logback | 1.5.1.RELEASE
数据库连接池 | Druid | 1.0.18
项目构建 | Maven | 3.5.2
监控 | SpringBoot-admin | 1.5.6