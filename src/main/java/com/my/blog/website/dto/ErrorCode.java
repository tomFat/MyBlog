package com.my.blog.website.dto;

/**
 * Created with IntelliJ IDEA.
 * Description: 错误提示
 * User: xxm
 * Date: 2018-02-01
 * Time: 23:09
 */
public interface ErrorCode {

    /**
     * 非法请求
     */
    String BAD_REQUEST = "BAD REQUEST";

}
